## Direcory overview 

The directory contains the following files:

* a directory with [tasks](tasks);
* a set of tasks [Tasks.set](Tasks.set);
* the [readme](README.md) file;
* an xml file with configuration of CPALockator: [benchmark-lockator-tm.xml](benchmark-lockator-tm.xml);
* an xml file with configuration of CPALockator with limited number of refinements: [benchmark-lockator-tm-limited.xml](benchmark-lockator-tm-limited.xml).

## Tasks description

The tasks are based on fixes in Linux stable in 2014 year.
The tasks have corresponding commit identifiers as names, where a suffix `-1` means a previous commit, like `~1` in git.
So, a task with a suffix `-1` is prepared before fix and a task without the suffix -- after it.

The verification tasks are prepared by [Klever](https://github.com/ldv-klever/klever) 2.1.dev160+g4df016cf8, linux:concurrency safety rule.
Build bases are collected by [Clade](https://github.com/17451k/clade) v3.0.2.
Some of the tasks contain manually prepared communicating scenario models for unsupported device driver types. Such model functions are emphasized by a suffix `_manual`.

Thread creation is modeled by a function `pthread_create` with default signature, which creates a single thread, or `pthread_create_N`, which creates several instances at once.
Locking is organized by functions `ldv_mutex_model_lock`/`ldv_mutex_model_unlock` and `ldv_spin_model_lock`/`ldv_spin_model_unlock`.

## Launches

Download CPAchecker [repository](https://svn.sosy-lab.org/software/cpachecker/trunk) and checkout to the branch `CPALockator-theory-with-races@r32609`.
To launch CPALockator with thread effects execute:
```
./scripts/benchmark.py benchmark-lockator-tm.xml
```
To launch CPALockator with thread effects and limited number of refinements execute:
```
./scripts/benchmark.py benchmark-lockator-tm-limited.xml
```
