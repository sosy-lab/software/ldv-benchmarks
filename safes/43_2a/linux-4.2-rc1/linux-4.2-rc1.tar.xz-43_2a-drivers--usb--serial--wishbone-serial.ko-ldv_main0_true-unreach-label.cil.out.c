/* Generated by CIL v. 1.5.1 */
/* print_CIL_Input is false */

#line 20 "include/uapi/asm-generic/int-ll64.h"
typedef unsigned char __u8;
#line 23 "include/uapi/asm-generic/int-ll64.h"
typedef unsigned short __u16;
#line 157 "include/linux/types.h"
typedef unsigned int gfp_t;
#line 32 "include/linux/mm_types.h"
struct kmem_cache;
#line 13 "include/linux/mod_devicetable.h"
typedef unsigned long kernel_ulong_t;
#line 39 "include/linux/mod_devicetable.h"
struct usb_device_id {
   __u16 match_flags ;
   __u16 idVendor ;
   __u16 idProduct ;
   __u16 bcdDevice_lo ;
   __u16 bcdDevice_hi ;
   __u8 bDeviceClass ;
   __u8 bDeviceSubClass ;
   __u8 bDeviceProtocol ;
   __u8 bInterfaceClass ;
   __u8 bInterfaceSubClass ;
   __u8 bInterfaceProtocol ;
   __u8 bInterfaceNumber ;
   kernel_ulong_t driver_info ;
};
#line 29 "include/linux/types.h"
typedef _Bool bool;
#line 361 "./arch/x86/include/asm/pgtable_types.h"
struct page;
#line 26 "/home/cluser/ldv/inst/kernel-rules/verifier/rcv.h"
extern void *ldv_undef_ptr(void) ;
#line 293 "include/linux/slab.h"
void *ldv_kmem_cache_alloc_20(struct kmem_cache *ldv_func_arg1 , gfp_t flags ) ;
#line 18 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/1659/dscv_tempdir/dscv/ri/43_2a/drivers/usb/serial/wishbone-serial.o.c.prepared"
void ldv_check_alloc_flags(gfp_t flags ) ;
#line 27 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/1659/dscv_tempdir/dscv/ri/43_2a/drivers/usb/serial/wishbone-serial.c"
struct usb_device_id  const  __mod_usb__id_table_device_table[2U]  ;
#line 113
extern void ldv_check_final_state(void) ;
#line 122
extern void ldv_initialize(void) ;
#line 128
extern int nondet_int(void) ;
#line 131 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/1659/dscv_tempdir/dscv/ri/43_2a/drivers/usb/serial/wishbone-serial.c"
int LDV_IN_INTERRUPT  ;
#line 134 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/1659/dscv_tempdir/dscv/ri/43_2a/drivers/usb/serial/wishbone-serial.c"
void main(void) 
{ 
  int tmp ;
  int tmp___0 ;

  {
#line 146
  LDV_IN_INTERRUPT = 1;
#line 155
  ldv_initialize();
#line 157
  goto ldv_32504;
  ldv_32503: 
#line 160
  tmp = nondet_int();
#line 160
  switch (tmp) {
  default: ;
#line 162
  goto ldv_32502;
  }
  ldv_32502: ;
  ldv_32504: 
#line 157
  tmp___0 = nondet_int();
#line 157
  if (tmp___0 != 0) {
#line 159
    goto ldv_32503;
  } else {

  }


#line 171
  ldv_check_final_state();
#line 174
  return;
}
}
#line 171 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/1659/dscv_tempdir/dscv/ri/43_2a/drivers/usb/serial/wishbone-serial.o.c.prepared"
void *ldv_kmem_cache_alloc_20(struct kmem_cache *ldv_func_arg1 , gfp_t flags ) 
{ 
  void *tmp ;

  {
#line 174
  ldv_check_alloc_flags(flags);
#line 175
  tmp = ldv_undef_ptr();
#line 175
  return (tmp);
}
}
#line 10 "/home/cluser/ldv/inst/kernel-rules/verifier/rcv.h"
__inline static void ldv_error(void) 
{ 


  {
  ERROR: ;
#line 12
  goto ERROR;
}
}
#line 25
extern int ldv_undef_int(void) ;
#line 7 "/home/cluser/ldv/inst/kernel-rules/kernel-model/ERR.inc"
bool ldv_is_err(void const   *ptr ) 
{ 


  {
#line 10
  return ((unsigned long )ptr > 2012UL);
}
}
#line 14 "/home/cluser/ldv/inst/kernel-rules/kernel-model/ERR.inc"
void *ldv_err_ptr(long error ) 
{ 


  {
#line 17
  return ((void *)(2012L - error));
}
}
#line 21 "/home/cluser/ldv/inst/kernel-rules/kernel-model/ERR.inc"
long ldv_ptr_err(void const   *ptr ) 
{ 


  {
#line 24
  return ((long )(2012UL - (unsigned long )ptr));
}
}
#line 28 "/home/cluser/ldv/inst/kernel-rules/kernel-model/ERR.inc"
bool ldv_is_err_or_null(void const   *ptr ) 
{ 
  bool tmp ;
  int tmp___0 ;

  {
#line 31
  if ((unsigned long )ptr == (unsigned long )((void const   *)0)) {
#line 31
    tmp___0 = 1;
  } else {
#line 31
    tmp = ldv_is_err(ptr);
#line 31
    if ((int )tmp) {
#line 31
      tmp___0 = 1;
    } else {
#line 31
      tmp___0 = 0;
    }
  }
#line 31
  return ((bool )tmp___0);
}
}
#line 20 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/1659/dscv_tempdir/rule-instrumentor/43_2a/common-model/ldv_common_model.c"
int ldv_spin  =    0;
#line 24 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/1659/dscv_tempdir/rule-instrumentor/43_2a/common-model/ldv_common_model.c"
void ldv_check_alloc_flags(gfp_t flags ) 
{ 


  {
#line 27
  if (ldv_spin != 0 && (flags & 16U) != 0U) {
#line 27
    ldv_error();
  } else {

  }
#line 31
  return;
}
}
#line 30
extern struct page *ldv_some_page(void) ;
#line 33 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/1659/dscv_tempdir/rule-instrumentor/43_2a/common-model/ldv_common_model.c"
struct page *ldv_check_alloc_flags_and_return_some_page(gfp_t flags ) 
{ 
  struct page *tmp ;

  {
#line 36
  if (ldv_spin != 0 && (flags & 16U) != 0U) {
#line 36
    ldv_error();
  } else {

  }
#line 38
  tmp = ldv_some_page();
#line 38
  return (tmp);
}
}
#line 42 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/1659/dscv_tempdir/rule-instrumentor/43_2a/common-model/ldv_common_model.c"
void ldv_check_alloc_nonatomic(void) 
{ 


  {
#line 45
  if (ldv_spin != 0) {
#line 45
    ldv_error();
  } else {

  }
#line 49
  return;
}
}
#line 49 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/1659/dscv_tempdir/rule-instrumentor/43_2a/common-model/ldv_common_model.c"
void ldv_spin_lock(void) 
{ 


  {
#line 52
  ldv_spin = 1;
#line 53
  return;
}
}
#line 56 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/1659/dscv_tempdir/rule-instrumentor/43_2a/common-model/ldv_common_model.c"
void ldv_spin_unlock(void) 
{ 


  {
#line 59
  ldv_spin = 0;
#line 60
  return;
}
}
#line 63 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/1659/dscv_tempdir/rule-instrumentor/43_2a/common-model/ldv_common_model.c"
int ldv_spin_trylock(void) 
{ 
  int is_lock ;

  {
#line 68
  is_lock = ldv_undef_int();
#line 70
  if (is_lock != 0) {
#line 73
    return (0);
  } else {
#line 78
    ldv_spin = 1;
#line 80
    return (1);
  }
}
}
